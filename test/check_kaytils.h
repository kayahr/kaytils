#ifndef CHECK_KAYTILS_H
#define CHECK_KAYTILS_H

#define START_SUITE(__suitename)\
static Suite *suite(void)\
{\
    Suite *s;\
    TCase *tc_core;\
    \
    s = suite_create(""# __suitename);\
    tc_core = tcase_create(""# __suitename);\
    suite_add_tcase(s, tc_core);

#define END_SUITE \
    return s;\
}\
\
int main(int argc, char *argv[])\
{\
    int nf;\
    SRunner *sr;\
\
    sr = srunner_create(suite());\
    srunner_run_all(sr, CK_ENV);\
    nf = srunner_ntests_failed(sr);\
    srunner_free(sr);\
    return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;\
}

#endif
