#include <stdlib.h>
#include <check.h>
#include <stdio.h>
#include "../src/str.h"
#include "../src/list.h"
#include "check_kaytils.h"

START_TEST(test_listCreateFree)
{
    char **list;
    size_t size;

    listCreate(list, &size);
    fail_unless(list != NULL, NULL);
    fail_unless(size == 0, NULL);
    listFree(list);
}
END_TEST

START_TEST(test_listFreeWithItems)
{
    char **list;
    char *string;
    size_t size;

    listCreate(list, &size);
    string = strCreate();
    strCopy(string, "Entry 1");
    listAdd(list, string, &size);
    string = strCreate();
    strCopy(string, "Entry 2");
    listAdd(list, string, &size);
    string = strCreate();
    strCopy(string, "Entry 3");
    listAdd(list, string, &size);
    listFreeWithItems(list, &size);
}
END_TEST

START_TEST(test_listClearAndFree)
{
    char **list;
    char *string;
    size_t size;

    listCreate(list, &size);
    string = strCreate();
    strCopy(string, "Entry 1");
    listAdd(list, string, &size);
    string = strCreate();
    strCopy(string, "Entry 2");
    listAdd(list, string, &size);
    string = strCreate();
    strCopy(string, "Entry 3");
    listAdd(list, string, &size);
    listClearAndFree(list, &size);
    fail_unless(size == 0, NULL);
    listFree(list);
}
END_TEST

START_TEST(test_listClear)
{
    char **list;
    size_t size;

    listCreate(list, &size);
    listAdd(list, "Entry 1", &size);
    listAdd(list, "Entry 2", &size);
    listAdd(list, "Entry 3", &size);
    listClear(list, &size);
    fail_unless(size == 0, NULL);
    listFree(list);
}
END_TEST

START_TEST(test_listAdd)
{
    char **list;
    size_t size;

    listCreate(list, &size);
    listAdd(list, "foobar", &size);
    fail_unless(size == 1, NULL);
    fail_unless(strEquals(list[0], "foobar"), NULL);
    listAdd(list, "barfoo", &size);
    fail_unless(size == 2, NULL);
    fail_unless(strEquals(list[1], "barfoo"), NULL);
    listFree(list);
}
END_TEST

START_TEST(test_listRemove)
{
    char **list;
    size_t size;

    listCreate(list, &size);
    listAdd(list, "foofoo", &size);
    listAdd(list, "foobar", &size);
    listAdd(list, "barfoo", &size);
    listAdd(list, "barbar", &size);
    listRemove(list, 1, &size);
    fail_unless(size == 3, NULL);
    fail_unless(strEquals(list[1], "barfoo"), NULL);
    listRemove(list, 0, &size);
    fail_unless(size == 2, NULL);
    fail_unless(strEquals(list[0], "barfoo"), NULL);
    listRemove(list, 1, &size);
    fail_unless(size == 1, NULL);
    fail_unless(strEquals(list[0], "barfoo"), NULL);
    listFree(list);
}
END_TEST

START_TEST(test_listRemoveAndFree)
{
    char **list;
    size_t size;

    listCreate(list, &size);
    listAdd(list, strDup("foofoo"), &size);
    listAdd(list, strDup("foobar"), &size);
    listAdd(list, strDup("barfoo"), &size);
    listAdd(list, strDup("barbar"), &size);
    listRemoveAndFree(list, 1, &size);
    fail_unless(size == 3, NULL);
    fail_unless(strEquals(list[1], "barfoo"), NULL);
    listRemoveAndFree(list, 0, &size);
    fail_unless(size == 2, NULL);
    fail_unless(strEquals(list[0], "barfoo"), NULL);
    listRemoveAndFree(list, 1, &size);
    fail_unless(size == 1, NULL);
    fail_unless(strEquals(list[0], "barfoo"), NULL);
    listFree(list);
}
END_TEST

START_TEST(test_listInsert)
{
    char **list;
    size_t size;

    listCreate(list, &size);
    listAdd(list, "foobar", &size);
    listAdd(list, "barfoo", &size);
    listInsert(list, 0, "barbar", &size);
    fail_unless(size == 3, NULL);
    fail_unless(strEquals(list[0], "barbar"), NULL);
    listInsert(list, 3, "foofoo", &size);
    fail_unless(size == 4, NULL);
    fail_unless(strEquals(list[3], "foofoo"), NULL);
    listInsert(list, 3, "foofoo2", &size);
    fail_unless(size == 5, NULL);
    fail_unless(strEquals(list[3], "foofoo2"), NULL);
    listFree(list);
}
END_TEST

START_SUITE(list)
{
    tcase_add_test(tc_core, test_listCreateFree);
    tcase_add_test(tc_core, test_listAdd);
    tcase_add_test(tc_core, test_listFreeWithItems);
    tcase_add_test(tc_core, test_listClear);
    tcase_add_test(tc_core, test_listClearAndFree);
    tcase_add_test(tc_core, test_listRemove);
    tcase_add_test(tc_core, test_listRemoveAndFree);
    tcase_add_test(tc_core, test_listInsert);
}
END_SUITE
