#include <stdlib.h>
#include <check.h>
#include <stdio.h>
#include <string.h>
#include "check_kaytils.h"
#include "../src/str.h"
#include "../src/list.h"

START_TEST(test_strCreateFree)
{
    char *string;

    string = strCreate();
    fail_unless(string != NULL, "string must not be NULL");
    fail_unless(strlen(string) == 0, "string must be empty");
    strFree(string);
}
END_TEST

START_TEST(test_strLength)
{
    char *string = "Hello World!";

    fail_unless(strLength(string) == 12, NULL);
}
END_TEST

START_TEST(test_strEquals)
{
    fail_unless(!strEquals("foo", "bar"), NULL);
    fail_unless(!strEquals("foo", NULL), NULL);
    fail_unless(strEquals("foo", "foo"), NULL);
    fail_unless(!strEquals("foo", "Foo"), NULL);
}
END_TEST

START_TEST(test_strEqualsIgnoreCase)
{
    fail_unless(!strEqualsIgnoreCase("foo", "bar"), NULL);
    fail_unless(!strEqualsIgnoreCase("foo", NULL), NULL);
    fail_unless(strEqualsIgnoreCase("foo", "foo"), NULL);
    fail_unless(strEqualsIgnoreCase("foo", "Foo"), NULL);
}
END_TEST

START_TEST(test_strCopy)
{
    char *string;

    string = strCreate();
    strCopy(string, "foo");
    fail_unless(strEquals(string, "foo"), NULL);
    strCopy(string, "bar");
    fail_unless(strEquals(string, "bar"), NULL);
    strCopy(string, "");
    fail_unless(strEquals(string, ""), NULL);
}
END_TEST

START_TEST(test_strAppend)
{
    char *string;

    string = strCreate();
    strCopy(string, "foo");
    strAppend(string, "bar");
    fail_unless(strEquals(string, "foobar"), NULL);
    strAppend(string, "");
    fail_unless(strEquals(string, "foobar"), NULL);
    strAppend(string, "foo");
    fail_unless(strEquals(string, "foobarfoo"), NULL);
}
END_TEST

START_TEST(test_strCopySub)
{
    char *string;

    string = strCreate();
    strCopySub(string, "foo", 0, 2);
    fail_unless(strEquals(string, "fo"), NULL);
    strCopySub(string, "foo", 1, 2);
    fail_unless(strEquals(string, "oo"), NULL);
    strCopySub(string, "foo", 2, 1);
    fail_unless(strEquals(string, "o"), NULL);
    strCopySub(string, "bar", 0, 1);
    fail_unless(strEquals(string, "b"), NULL);
    strCopySub(string, "foo", 0, 0);
    fail_unless(strEquals(string, ""), NULL);
    strCopySub(string, "foo", 0, 3);
    fail_unless(strEquals(string, "foo"), NULL);
}
END_TEST

START_TEST(test_strAppendSub)
{
    char *string;

    string = strCreate();
    strCopy(string, "foo");
    strAppendSub(string, "bar", 0, 2);
    fail_unless(strEquals(string, "fooba"), NULL);
    strAppendSub(string, "", 0, 0);
    fail_unless(strEquals(string, "fooba"), NULL);
    strAppendSub(string, "bar", 0, 0);
    fail_unless(strEquals(string, "fooba"), NULL);
    strAppendSub(string, "foo", 0, 3);
    fail_unless(strEquals(string, "foobafoo"), NULL);
    strAppendSub(string, "bar", 1, 2);
    fail_unless(strEquals(string, "foobafooar"), NULL);
    strAppendSub(string, "bar", 2, 1);
    fail_unless(strEquals(string, "foobafooarr"), NULL);
}
END_TEST

START_TEST(test_strFindFirst)
{
    fail_unless(strFindFirst("foobarfoo", "bar") == 3, NULL);
    fail_unless(strFindFirst("foobarfoo", "foo") == 0, NULL);
    fail_unless(strFindFirst("foobarfoo", "bla") == -1, NULL);
    fail_unless(strFindFirst("ffoobarfoo", "f") == 0, NULL);
}
END_TEST

START_TEST(test_strFindLast)
{
    fail_unless(strFindLast("foobarfoo", "bar") == 3, NULL);
    fail_unless(strFindLast("foobarfoo", "foo") == 6, NULL);
    fail_unless(strFindLast("foobarfoo", "bla") == -1, NULL);
    fail_unless(strFindLast("foobarfoo", "o") == 8, NULL);
}
END_TEST

START_TEST(test_strFindFirstChar)
{
    fail_unless(strFindFirstChar("foobarfoo", 'o') == 1, NULL);
    fail_unless(strFindFirstChar("foobarfoo", 'f') == 0, NULL);
    fail_unless(strFindFirstChar("foobarfoz", 'z') == 8, NULL);
    fail_unless(strFindFirstChar("foobarfoo", 'z') == -1, NULL);
}
END_TEST

START_TEST(test_strFindLastChar)
{
    fail_unless(strFindLastChar("foobarfoo", 'o') == 8, NULL);
    fail_unless(strFindLastChar("foobarfoo", 'f') == 6, NULL);
    fail_unless(strFindLastChar("zoobarfoo", 'z') == 0, NULL);
    fail_unless(strFindLastChar("foobarfoo", 'z') == -1, NULL);
}
END_TEST

START_TEST(test_strFindFirstOf)
{
    fail_unless(strFindFirstOf("foobarfoo", "fo") == 0, NULL);
    fail_unless(strFindFirstOf("foobarfoo", "ob") == 1, NULL);
    fail_unless(strFindFirstOf("foobarfoz", "yz") == 8, NULL);
    fail_unless(strFindFirstOf("foobarfoo", "yz") == -1, NULL);
}
END_TEST

START_TEST(test_strFindLastOf)
{
    fail_unless(strFindLastOf("foobarfoo", "fo") == 8, NULL);
    fail_unless(strFindLastOf("foobarfoo", "fb") == 6, NULL);
    fail_unless(strFindLastOf("zoobarfoo", "yz") == 0, NULL);
    fail_unless(strFindLastOf("foobarfoo", "yz") == -1, NULL);
}
END_TEST

START_TEST(test_strFindFirstNotOf)
{
    fail_unless(strFindFirstNotOf("foobarfoo", "fo") == 3, NULL);
    fail_unless(strFindFirstNotOf("foobarfoo", "ob") == 0, NULL);
    fail_unless(strFindFirstNotOf("foobarfoo", "foobar") == -1, NULL);
}
END_TEST

START_TEST(test_strFindLastNotOf)
{
    fail_unless(strFindLastNotOf("foobarfoo", "fo") == 5, NULL);
    fail_unless(strFindLastNotOf("zoobarfoo", "foobar") == 0, NULL);
    fail_unless(strFindLastNotOf("foobarfoo", "foobar") == -1, NULL);
}
END_TEST

START_TEST(test_strCount)
{
    fail_unless(strCount("foobarfoo", "foo") == 2, NULL);
    fail_unless(strCount("foobarfoo", "bar") == 1, NULL);
    fail_unless(strCount("foo", "foo") == 1, NULL);
    fail_unless(strCount("foo", "bar") == 0, NULL);
}
END_TEST

START_TEST(test_strCountChar)
{
    fail_unless(strCountChar("foobarfoo", 'f') == 2, NULL);
    fail_unless(strCountChar("foobarfoo", 'b') == 1, NULL);
    fail_unless(strCountChar("ff", 'f') == 2, NULL);
    fail_unless(strCountChar("f", 'f') == 1, NULL);
    fail_unless(strCountChar("f", 'b') == 0, NULL);
}
END_TEST

START_TEST(test_strReplace)
{
    char *string;

    string = strCreate();
    strCopy(string, "foobarfoo");
    strReplace(string, "foo", "xyz");
    fail_unless(strEquals(string, "xyzbarxyz"), NULL);
    strCopy(string, "foobarfoo");
    strReplace(string, "bar", "xyz");
    fail_unless(strEquals(string, "fooxyzfoo"), NULL);
    strCopy(string, "foobar");
    strReplace(string, "foo", "xyz");
    fail_unless(strEquals(string, "xyzbar"), NULL);
    strReplace(string, "foo", "xyz");
    fail_unless(strEquals(string, "xyzbar"), NULL);
    strReplace(string, "xyz", "");
    fail_unless(strEquals(string, "bar"), NULL);
    strReplace(string, "bar", "foo");
    fail_unless(strEquals(string, "foo"), NULL);
    strReplace(string, "foo", "");
    fail_unless(strEquals(string, ""), NULL);
    strReplace(string, "foo", "bar");
    fail_unless(strEquals(string, ""), NULL);
}
END_TEST

START_TEST(test_strReplaceChar)
{
    char *string;

    string = strCreate();
    strCopy(string, "foobar");
    strReplaceChar(string, 'o', 'i');
    fail_unless(strEquals(string, "fiibar"), NULL);
    strReplaceChar(string, 'b', 'p');
    fail_unless(strEquals(string, "fiipar"), NULL);
    strReplaceChar(string, 'b', 'x');
    fail_unless(strEquals(string, "fiipar"), NULL);
}
END_TEST

START_TEST(test_strDelete)
{
    char *string;

    string = strCreate();
    strCopy(string, "foobar");
    strDelete(string, 1, 4);
    fail_unless(strEquals(string, "fr"), NULL);
    strCopy(string, "foobar");
    strDelete(string, 1, 5);
    fail_unless(strEquals(string, "f"), NULL);
    strCopy(string, "foobar");
    strDelete(string, 0, 5);
    fail_unless(strEquals(string, "r"), NULL);
    strCopy(string, "foobar");
    strDelete(string, 0, 6);
    fail_unless(strEquals(string, ""), NULL);
    strCopy(string, "foobar");
    strDelete(string, 0, 0);
    fail_unless(strEquals(string, "foobar"), NULL);
    strCopy(string, "foobar");
    strDelete(string, 5, 0);
    fail_unless(strEquals(string, "foobar"), NULL);
}
END_TEST

START_TEST(test_strTrimRight)
{
    char *string;

    string = strCreate();
    strCopy(string, " \n\r\tfoobar");
    strTrimRight(string);
    fail_unless(strEquals(string, " \n\r\tfoobar"), NULL);
    strCopy(string, "foobar \n\r\t");
    strTrimRight(string);
    fail_unless(strEquals(string, "foobar"), NULL);
    strCopy(string, "foobar");
    strTrimRight(string);
    fail_unless(strEquals(string, "foobar"), NULL);
    strCopy(string, " \n\r\tfoobar \r\t\n");
    strTrimRight(string);
    fail_unless(strEquals(string, " \n\r\tfoobar"), NULL);
    strCopy(string, " \n\r\tfoo\t \n\rbar \r\t\n");
    strTrimRight(string);
    fail_unless(strEquals(string, " \n\r\tfoo\t \n\rbar"), NULL);
}
END_TEST


START_TEST(test_strTrimLeft)
{
    char *string;

    string = strCreate();
    strCopy(string, " \n\r\tfoobar");
    strTrimLeft(string);
    fail_unless(strEquals(string, "foobar"), NULL);
    strCopy(string, "foobar \n\r\t");
    strTrimLeft(string);
    fail_unless(strEquals(string, "foobar \n\r\t"), NULL);
    strCopy(string, "foobar");
    strTrimLeft(string);
    fail_unless(strEquals(string, "foobar"), NULL);
    strCopy(string, " \n\r\tfoobar \r\t\n");
    strTrimLeft(string);
    fail_unless(strEquals(string, "foobar \r\t\n"), NULL);
    strCopy(string, " \n\r\tfoo\t \n\rbar \r\t\n");
    strTrimLeft(string);
    fail_unless(strEquals(string, "foo\t \n\rbar \r\t\n"), NULL);
}
END_TEST

START_TEST(test_strTrim)
{
    char *string;

    string = strCreate();
    strCopy(string, " \n\r\tfoobar");
    strTrim(string);
    fail_unless(strEquals(string, "foobar"), NULL);
    strCopy(string, "foobar \n\r\t");
    strTrim(string);
    fail_unless(strEquals(string, "foobar"), NULL);
    strCopy(string, "foobar");
    strTrim(string);
    fail_unless(strEquals(string, "foobar"), NULL);
    strCopy(string, " \n\r\tfoobar \r\t\n");
    strTrim(string);
    fail_unless(strEquals(string, "foobar"), NULL);
    strCopy(string, " \n\r\tfoo\t \n\rbar \r\t\n");
    strTrim(string);
    fail_unless(strEquals(string, "foo\t \n\rbar"), NULL);
}
END_TEST

START_TEST(test_strSplit)
{
    char *string;
    char **list;
    size_t size;

    string = strCreate();
    strCopy(string, "foo bar hurz furz");
    list = strSplit(" ", string, -1, &size);
    fail_unless(size == 4, NULL);
    fail_unless(strEquals(list[0], "foo"), NULL);
    fail_unless(strEquals(list[1], "bar"), NULL);
    fail_unless(strEquals(list[2], "hurz"), NULL);
    fail_unless(strEquals(list[3], "furz"), NULL);
    listFreeWithItems(list, &size);
    list = strSplit(" ", string, 0, &size);
    fail_unless(size == 0, NULL);
    listFreeWithItems(list, &size);
    list = strSplit(" ", string, 2, &size);
    fail_unless(size == 2, NULL);
    fail_unless(strEquals(list[0], "foo"), NULL);
    fail_unless(strEquals(list[1], "bar hurz furz"), NULL);
    listFreeWithItems(list, &size);
    strCopy(string, "foo bar hurz bar furz");
    list = strSplit("bar", string, 3, &size);
    fail_unless(size == 3, NULL);
    fail_unless(strEquals(list[0], "foo "), NULL);
    fail_unless(strEquals(list[1], " hurz "), NULL);
    fail_unless(strEquals(list[2], " furz"), NULL);
    listFreeWithItems(list, &size);
}
END_TEST

START_TEST(test_strJoin)
{
    char **list;
    size_t size;

    listCreate(list, &size);
    listAdd(list, "foo", &size);
    listAdd(list, "bar", &size);
    listAdd(list, "xyz", &size);
    fail_unless(strEquals(strJoin(" ", list, size), "foo bar xyz"), NULL);
    fail_unless(strEquals(strJoin("()", list, size), "foo()bar()xyz"), NULL);
    listRemove(list, 1, &size);
    fail_unless(strEquals(strJoin(" ", list, size), "foo xyz"), NULL);
    listRemove(list, 1, &size);
    fail_unless(strEquals(strJoin(" ", list, size), "foo"), NULL);
    listRemove(list, 0, &size);
    fail_unless(strEquals(strJoin(" ", list, size), ""), NULL);
    listFree(list);
}
END_TEST

START_TEST(test_strEndsWith)
{
    fail_unless(!strEndsWith("foo.bar", ".foo"), NULL);
    fail_unless(strEndsWith("foo.bar", ".bar"), NULL);
    fail_unless(!strEndsWith("foo.bar", "o"), NULL);
    fail_unless(strEndsWith("foo.bar", "r"), NULL);
    fail_unless(strEndsWith("foo.bar", "oo.bar"), NULL);
    fail_unless(strEndsWith("foo.bar", "foo.bar"), NULL);
    fail_unless(!strEndsWith("foo.bar", "xoo.bar"), NULL);
    fail_unless(!strEndsWith("fo", "foo"), NULL);
    fail_unless(!strEndsWith("", "foo"), NULL);
}
END_TEST

START_TEST(test_strEndsWithIgnoreCase)
{
    fail_unless(!strEndsWithIgnoreCase("foo.bar", ".Foo"), NULL);
    fail_unless(strEndsWithIgnoreCase("foo.bar", ".bAr"), NULL);
    fail_unless(!strEndsWithIgnoreCase("foo.bar", "o"), NULL);
    fail_unless(strEndsWithIgnoreCase("foo.bar", "R"), NULL);
    fail_unless(strEndsWithIgnoreCase("foo.bar", "oO.Bar"), NULL);
    fail_unless(strEndsWithIgnoreCase("foo.bar", "fOo.baR"), NULL);
    fail_unless(!strEndsWithIgnoreCase("foo.bar", "xoo.bar"), NULL);
    fail_unless(!strEndsWithIgnoreCase("fo", "foo"), NULL);
    fail_unless(!strEndsWithIgnoreCase("", "foo"), NULL);
}
END_TEST

START_TEST(test_strStartsWith)
{
    fail_unless(!strStartsWith("foo.bar", "bar."), NULL);
    fail_unless(strStartsWith("foo.bar", "foo."), NULL);
    fail_unless(!strStartsWith("foo.bar", "b"), NULL);
    fail_unless(strStartsWith("foo.bar", "f"), NULL);
    fail_unless(strStartsWith("foo.bar", "foo.ba"), NULL);
    fail_unless(strStartsWith("foo.bar", "foo.bar"), NULL);
    fail_unless(!strStartsWith("foo.bar", "foo.bax"), NULL);
    fail_unless(!strStartsWith("fo", "foo"), NULL);
    fail_unless(!strStartsWith("", "foo"), NULL);
}
END_TEST

START_TEST(test_strStartsWithIgnoreCase)
{
    fail_unless(!strStartsWithIgnoreCase("foo.bar", "Bar."), NULL);
    fail_unless(strStartsWithIgnoreCase("foo.bar", "Foo."), NULL);
    fail_unless(!strStartsWithIgnoreCase("foo.bar", "B"), NULL);
    fail_unless(strStartsWithIgnoreCase("foo.bar", "F"), NULL);
    fail_unless(strStartsWithIgnoreCase("foo.bar", "fOo.ba"), NULL);
    fail_unless(strStartsWithIgnoreCase("foo.bar", "foo.Bar"), NULL);
    fail_unless(!strStartsWithIgnoreCase("foo.bar", "foo.bAx"), NULL);
    fail_unless(!strStartsWithIgnoreCase("fo", "foo"), NULL);
    fail_unless(!strStartsWithIgnoreCase("", "foo"), NULL);
}
END_TEST

START_SUITE(str)
{
    tcase_add_test(tc_core, test_strCreateFree);
    tcase_add_test(tc_core, test_strLength);
    tcase_add_test(tc_core, test_strEquals);
    tcase_add_test(tc_core, test_strEqualsIgnoreCase);
    tcase_add_test(tc_core, test_strCopy);
    tcase_add_test(tc_core, test_strAppend);
    tcase_add_test(tc_core, test_strCopySub);
    tcase_add_test(tc_core, test_strAppendSub);
    tcase_add_test(tc_core, test_strFindFirst);
    tcase_add_test(tc_core, test_strFindLast);
    tcase_add_test(tc_core, test_strFindFirstChar);
    tcase_add_test(tc_core, test_strFindLastChar);
    tcase_add_test(tc_core, test_strFindFirstOf);
    tcase_add_test(tc_core, test_strFindLastOf);
    tcase_add_test(tc_core, test_strFindFirstNotOf);
    tcase_add_test(tc_core, test_strFindLastNotOf);
    tcase_add_test(tc_core, test_strCount);
    tcase_add_test(tc_core, test_strCountChar);
    tcase_add_test(tc_core, test_strReplace);
    tcase_add_test(tc_core, test_strReplaceChar);
    tcase_add_test(tc_core, test_strDelete);
    tcase_add_test(tc_core, test_strTrimRight);
    tcase_add_test(tc_core, test_strTrimLeft);
    tcase_add_test(tc_core, test_strTrim);
    tcase_add_test(tc_core, test_strSplit);
    tcase_add_test(tc_core, test_strJoin);
    tcase_add_test(tc_core, test_strEndsWith);
    tcase_add_test(tc_core, test_strEndsWithIgnoreCase);
    tcase_add_test(tc_core, test_strStartsWith);
    tcase_add_test(tc_core, test_strStartsWithIgnoreCase);
}
END_SUITE
